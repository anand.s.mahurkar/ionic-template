import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagesliderComponent } from './imageslider.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [ImagesliderComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ImagesliderComponent]
})
export class ImagesliderModule { }
