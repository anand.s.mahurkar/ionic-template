import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionbuttonComponent } from './actionbutton.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [ActionbuttonComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ActionbuttonComponent]
})
export class ActionbuttonModule { }
